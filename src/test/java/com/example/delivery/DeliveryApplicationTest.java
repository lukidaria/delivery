package com.example.delivery;

import com.example.delivery.deliveryApp.config.ServiceConnectionIntegrationTest;
import org.springframework.boot.SpringApplication;

class DeliveryApplicationTest {

    public static void main(String[] args) {
        SpringApplication.from(DeliveryApplication::main)
            .with(ServiceConnectionIntegrationTest.class)
            .run(args);
    }

}