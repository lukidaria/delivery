package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Order;

import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.OrderStatus;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Weight;
import com.example.delivery.deliveryApp.Core.Ports.OrderRepository;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.AppTestConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@SpringBootTest(classes = AppTestConfiguration.class)
public class OrderRepositoryTest {

    @Autowired
    private OrderRepository orderRepository;

    @Test
    @Sql({"/psql/order.sql"})
    @Transactional
    void save() {
        Order order = new Order(UUID.randomUUID(), UUID.fromString("8ed12e2e-7052-42cd-930f-5e7d60fb20d7"),
                Location.MAX_LOCATION, new Weight(2), OrderStatus.CREATED);
        var actual = orderRepository.save(order);
        Assertions.assertNotNull(actual.getId());
        Assertions.assertEquals(UUID.fromString("8ed12e2e-7052-42cd-930f-5e7d60fb20d7"), actual.getCourierId());
    }

}
