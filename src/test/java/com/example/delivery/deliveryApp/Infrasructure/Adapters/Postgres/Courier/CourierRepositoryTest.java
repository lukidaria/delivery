package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier;

import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Courier;
import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.CourierStatus;
import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Transport;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import com.example.delivery.deliveryApp.Core.Ports.CourierRepository;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.AppTestConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@SpringBootTest(classes = AppTestConfiguration.class)
public class CourierRepositoryTest {

    @Autowired
    private CourierRepository courierRepository;

    @Test
    @Sql("classpath:psql/courier.sql")
    @Transactional
    void save() {
        Courier courier = new Courier(UUID.fromString("8ed12e2e-7052-42cd-930f-5e7d60fb20d7"), "Иванов Иван иванович",
                Transport.SCOOTER, new Location(1, 1), CourierStatus.READY);
        var actual = courierRepository.save(courier);
        Assertions.assertNotNull(actual.getId());
        Assertions.assertEquals("Иванов Иван иванович", actual.getName());
    }

    @Test
    @Sql({"classpath:psql/courier.sql","classpath:psql/insert_couriers.sql"})
    @Transactional
    void getAllReady() {
        var actualCouriers = courierRepository.getAllReady();
        Assertions.assertNotNull(actualCouriers);
        Assertions.assertEquals(1, actualCouriers.size());
    }

}
