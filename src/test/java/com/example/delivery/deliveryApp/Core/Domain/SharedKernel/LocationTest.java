package com.example.delivery.deliveryApp.Core.Domain.SharedKernel;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LocationTest {
    @Test
    void create() {
        var actual = Location.create(2, 9);
        assertAll(
                () -> assertEquals(2, actual.xValue()),
                () -> assertEquals(9, actual.yValue())
        );
    }

    @Test
    void distanceTo() {
        var currentLocation = Location.create(2, 3);
        var targetLocation = Location.create(8, 9);
        var actual = currentLocation.distanceToFull(targetLocation);
        assertEquals(12, actual);
    }
}