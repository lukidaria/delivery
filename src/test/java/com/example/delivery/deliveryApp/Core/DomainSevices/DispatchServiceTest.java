package com.example.delivery.deliveryApp.Core.DomainSevices;

import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Courier;
import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.CourierStatus;
import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Transport;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.OrderStatus;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Weight;
import com.example.delivery.deliveryApp.Core.DomainServices.DispatchService;
import com.example.delivery.deliveryApp.Core.Ports.CourierRepository;
import com.example.delivery.deliveryApp.Core.Ports.OrderRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {DispatchService.class})
public class DispatchServiceTest {

    @Autowired
    private DispatchService dispatchService;

    @MockBean
    private CourierRepository courierRepository;

    @MockBean
    private OrderRepository orderRepository;

    @Test
    void dispatchTest() {
        var order = new Order(UUID.fromString("137e4e01-963c-4973-bebc-b957db6368d4"),
                UUID.fromString("137e4e01-963c-4973-bebc-b957db6368d1"),
                new Location(3, 5), new Weight(2), OrderStatus.CREATED);
        when(orderRepository.getById(any())).thenReturn(order);
        var courier = new Courier(UUID.fromString("8ed12e2e-7052-42cd-930f-5e7d60fb20d7"), "Иванов Иван иванович",
                Transport.SCOOTER, new Location(1, 1), CourierStatus.READY);
        when(courierRepository.getAllReady()).thenReturn(List.of(courier));
        var actual = dispatchService.dispatch(order, List.of(courier));
        Assertions.assertNotNull(actual.getId());
    }
}
