package com.example.delivery.deliveryApp.Core.Domain.SharedKernel;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeightTest {

    @Test
    void createCorrect() {
        var actual = new Weight(2);
        assertEquals(2, actual.value());
    }
    @Test
    void createNotCorrect() {
        var actual = assertThrows(IllegalArgumentException.class, () -> new Weight(-2));
        assertEquals("Value should be positive.", actual.getMessage());
    }
    @Test
    void testEquals() {
        var first = new Weight(2);
        var second = new Weight(2);
        var actual = first.equals(second);
        assertTrue(actual);
    }

    @Test
    void compareTo() {
        var first = new Weight(2);
        var second = new Weight(2);
        var actual = first.compareTo(second);
        assertEquals(0, actual);
    }
}