package com.example.delivery.deliveryApp.Api.adapters;

import com.example.delivery.deliveryApp.Api.adapters.http.Controller;
import com.example.delivery.deliveryApp.Api.adapters.http.mapper.OrderInfoHttpMapper;
import com.example.delivery.deliveryApp.Core.Application.command.assignOrder.AssignOrderHandler;
import com.example.delivery.deliveryApp.Core.Application.query.getOrders.GetOrderByIdHandler;
import com.example.delivery.deliveryApp.Core.Application.query.getOrders.OrderInfoDto;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = {
        Controller.class,
        OrderInfoHttpMapper.class
})
@AutoConfigureMockMvc(addFilters = false)
public class ControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GetOrderByIdHandler getOrderInfoHandler;

    @MockBean
    private AssignOrderHandler assignOrderHandler;

    private OrderInfoDto orderInfoDto;

    @BeforeEach
    public void setUp() {
        orderInfoDto = new OrderInfoDto(UUID
                .fromString("137e4e01-963c-4973-bebc-b957db6368d4")
                , UUID.fromString("137e4e01-963c-4973-bebc-b957db6368d7"), new Location(1, 1));

    }

    @Test
    void getOrder() throws Exception {
        when(getOrderInfoHandler.get(any())).thenReturn(orderInfoDto);

        mockMvc.perform(get("/orders/v1/{orderId}", orderInfoDto.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(orderInfoDto.getId().toString()))
                .andExpect(jsonPath("$.courierId").value(orderInfoDto.getCourierId().toString()))
                .andExpect(jsonPath("$.courierLocation.abscissaValue").value(orderInfoDto.getCourierLocation().xValue()))
                .andExpect(jsonPath("$.courierLocation.ordinateValue").value(orderInfoDto.getCourierLocation().yValue()));
    }

    @Test
    void assignOrder() throws Exception{
        mockMvc.perform(post("/orders/v1/{orderId}/assign", orderInfoDto.getId())
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        verify(assignOrderHandler).assignTo(any());
    }


}
