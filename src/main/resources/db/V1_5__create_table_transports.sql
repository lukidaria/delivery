CREATE TABLE IF NOT EXISTS transports
(
    id          serial primary key,

    name        varchar (64),
    speed       smallint,
    capacity    smallint

);


COMMENT ON COLUMN transports.name IS 'Name of transport';
--rollback COMMENT ON COLUMN transports.name IS NULL;

COMMENT ON COLUMN transports.speed IS 'Transport speed';
--rollback COMMENT ON COLUMN transports.speed IS NULL;

COMMENT ON COLUMN transports.capacity IS 'Transport capacity';
--rollback COMMENT ON COLUMN transports.capacity IS NULL;

COMMENT ON TABLE transports IS 'Transports table';
--rollback COMMENT ON TABLE transports IS NULL;


INSERT INTO transports (id, name, speed, capacity)
VALUES (1, 'Pedestrian', 1, 1),
       (2, 'Bicycle', 2, 4),
       (3, 'Scooter', 3, 6),
       (3, 'Car', 4, 8)
ON CONFLICT DO NOTHING;
