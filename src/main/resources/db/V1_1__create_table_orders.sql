CREATE TABLE IF NOT EXISTS orders
(
    id                     uuid                    not null
    constraint pk_order primary key,

    created_at             timestamp default now() not null,
    modified_at            timestamp default now() not null,
    version                integer   default 0     not null,

    status_id              smallint,
    courier_id             uuid,
    delivery_location      varchar(64),
    weight                 smallint

);


COMMENT ON COLUMN orders.status_id IS 'Order execution status ID';
--rollback COMMENT ON COLUMN orders.status_id IS NULL;

 COMMENT ON COLUMN orders.courier_id IS 'Courier ID assigned whose is delivering the order';
--rollback COMMENT ON COLUMN orders.courier_id IS NULL;

 COMMENT ON COLUMN orders.delivery_location IS 'Location where the order needs to be delivered';
--rollback COMMENT ON COLUMN orders.delivery_location IS NULL;

 COMMENT ON COLUMN orders.weight IS 'Order weight';
--rollback COMMENT ON COLUMN orders.weight IS NULL;

COMMENT ON TABLE orders IS 'Order table';
--rollback COMMENT ON TABLE orders  IS NULL;
