package com.example.delivery.deliveryApp.Api.adapters.http;

import com.example.delivery.deliveryApp.Api.adapters.http.api.ApiApi;
import com.example.delivery.deliveryApp.Api.adapters.http.mapper.OrderInfoHttpMapper;
import com.example.delivery.deliveryApp.Api.adapters.http.model.Courier;
import com.example.delivery.deliveryApp.Api.adapters.http.model.Order;
import com.example.delivery.deliveryApp.Core.Application.command.assignOrder.AssignOrderCommand;
import com.example.delivery.deliveryApp.Core.Application.command.assignOrder.AssignOrderHandler;
import com.example.delivery.deliveryApp.Core.Application.command.createOrder.CreateOrderCommand;
import com.example.delivery.deliveryApp.Core.Application.command.createOrder.CreateOrderHandler;
import com.example.delivery.deliveryApp.Core.Application.command.finishWork.FinishWorkCommand;
import com.example.delivery.deliveryApp.Core.Application.command.finishWork.FinishWorkHandler;
import com.example.delivery.deliveryApp.Core.Application.command.moveToLocation.MoveToLocationCommand;
import com.example.delivery.deliveryApp.Core.Application.command.moveToLocation.MoveToLocationHandler;
import com.example.delivery.deliveryApp.Core.Application.command.startWork.StartWorkCommand;
import com.example.delivery.deliveryApp.Core.Application.command.startWork.StartWorkHandler;
import com.example.delivery.deliveryApp.Core.Application.query.getCouriers.AllBusyCouriers;
import com.example.delivery.deliveryApp.Core.Application.query.getOrders.AllNotCompletedOrders;
import com.example.delivery.deliveryApp.Core.Application.query.getOrders.GetOrderByIdHandler;
import com.example.delivery.deliveryApp.Core.Application.query.getOrders.GetOrderByIdQuery;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.OrderStatus;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Weight;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
public class Controller implements ApiApi {

    private final StartWorkHandler startWorkHandler;
    private final FinishWorkHandler finishWorkHandler;
    private final MoveToLocationHandler moveToLocationHandler;
    private final AllBusyCouriers allBusyCouriers;

    private final AssignOrderHandler assignOrderHandler;
    private final GetOrderByIdHandler getOrderByIdHandler;
    private final OrderInfoHttpMapper orderMapperHttp;
    private final CreateOrderHandler createOrderHandler;
    private final AllNotCompletedOrders allNotCompletedOrders;

    @Override
    public ResponseEntity<Void> stopWork(UUID courierId) {
        log.info("call method finishWorkingDay");
        finishWorkHandler.finish(new FinishWorkCommand(courierId));
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<Void> moveToLocation(UUID courierId) {
        log.info("call method moveToLocation");
        moveToLocationHandler.moveTo(new MoveToLocationCommand(courierId));
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<List<Courier>> getCouriers() {
        var couriers = allBusyCouriers.get();
        return ApiApi.super.getCouriers();
    }

    @Override
    public ResponseEntity<Void> startWork(UUID courierId) {
        log.info("call method startWorkingDay");
        startWorkHandler.start(new StartWorkCommand(courierId));
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<Void> assignOrder(UUID orderId) {
        log.info("call method assignOrder");
        assignOrderHandler.assignTo(new AssignOrderCommand(orderId));
        return ResponseEntity
                .status(HttpStatus.OK)
                .build();
    }

    @Override
    public ResponseEntity<Void> createOrder() {
        log.info("call method createOrder");
        createOrderHandler.create(new CreateOrderCommand(
                new com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order(UUID.randomUUID(),
                        UUID.fromString("bf79a004-56d7-4e5f-a21c-0a9e5e08d10d"),
                        new Location(1, 1),
                        new Weight(2),
                        OrderStatus.CREATED)));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Order>> getOrders() {
        var dtos = allNotCompletedOrders.get();
        var orders = orderMapperHttp.toApiListOrders(dtos);
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @GetMapping("/api/v1/order/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable("id") UUID orderId) {
        log.info("call method getOrder");
        var orderInfo = getOrderByIdHandler.get(new GetOrderByIdQuery(orderId));
        var order = orderMapperHttp.toApiOrder(orderInfo);
        return new ResponseEntity<>(order, HttpStatus.OK);

    }
}
