package com.example.delivery.deliveryApp.Api.adapters.http.mapper;

import com.example.delivery.deliveryApp.Api.adapters.http.model.Location;
import com.example.delivery.deliveryApp.Api.adapters.http.model.Order;
import com.example.delivery.deliveryApp.Core.Application.query.getOrders.OrderInfoDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderInfoHttpMapper {

    public Order toApiOrder(OrderInfoDto orderDto) {
        var order = new Order(orderDto.getId(),
                new Location(orderDto.getCourierLocation().xValue(),
                orderDto.getCourierLocation().yValue()));
        return order;
    }

    public List<Order> toApiListOrders(List<OrderInfoDto> infoDtoList){
        return infoDtoList.stream()
                .map(infoDto -> {
                    Order order = new Order();
                    order.setId(infoDto.getId());
                    order.setLocation(new Location(infoDto.getCourierLocation().xValue(),
                            infoDto.getCourierLocation().yValue()));
                    return order;
                }).toList();
    }

}
