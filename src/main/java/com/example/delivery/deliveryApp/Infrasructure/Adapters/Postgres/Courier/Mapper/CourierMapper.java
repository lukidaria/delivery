package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier.Mapper;

import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier.Entity.CourierEntity;
import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Courier;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;
import java.util.Optional;


@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CourierMapper extends Mappable<CourierEntity, Courier> {

    Courier toDomain(CourierEntity entity);

    CourierEntity toEntity(Courier domain);

    List<Courier> toDomain(List<CourierEntity> entities);

    default CourierEntity updateEntity(Courier entity, CourierEntity targetEntity) {
        Optional.of(entity)
                .map(Courier::getName).ifPresent(targetEntity::setName);
        Optional.of(entity)
                .map(Courier::getCourierStatus).ifPresent(targetEntity::setCourierStatus);
        Optional.of(entity)
                .map(Courier::getTransport).ifPresent(targetEntity::setTransport);
        Optional.of(entity)
                .map(Courier::getLocation).ifPresent(targetEntity::setLocation);
        return targetEntity;
    }

}

