package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Order.Repository;

import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.OrderStatus;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Order.Entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface OrderJpaRepository extends JpaRepository<OrderEntity, UUID> {

    List<OrderEntity> findAllByOrderStatus(OrderStatus orderStatus);
    OrderEntity findByCourierId(UUID courierId);
}
