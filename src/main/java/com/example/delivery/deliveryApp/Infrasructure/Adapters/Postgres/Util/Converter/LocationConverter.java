package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter;

import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;
import org.springframework.stereotype.Component;

@Component
@Converter
public class LocationConverter implements AttributeConverter<Location, String> {

    private static final String PATTERN = "%d,%d";
    private static final String COMMA = ",";


    @Override
    public String convertToDatabaseColumn(Location attribute) {
        try {
            return PATTERN.formatted(attribute.xValue(), attribute.yValue());
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Location convertToEntityAttribute(String dbData) {
        var dbDataArray = dbData.split(COMMA);
        var abscissaValue = Integer.parseInt(dbDataArray[0]);
        var ordinateValue = Integer.parseInt(dbDataArray[1]);
        return Location.create(abscissaValue, ordinateValue);
    }
}
