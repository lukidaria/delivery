package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter;

import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Weight;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter
public class WeightConverter implements AttributeConverter<Weight, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Weight attribute) {
        try {
            return attribute.value();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Weight convertToEntityAttribute(Integer value) {
        return new Weight(value);
    }
}
