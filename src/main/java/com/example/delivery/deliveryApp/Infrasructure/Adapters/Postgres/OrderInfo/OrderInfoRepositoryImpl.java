package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.OrderInfo;

import com.example.delivery.deliveryApp.Core.Application.query.getOrders.OrderInfoDto;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.OrderStatus;
import com.example.delivery.deliveryApp.Core.Ports.OrderInfoRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
public class OrderInfoRepositoryImpl implements OrderInfoRepository {

    private final EntityManager manager;

    @Override
    public OrderInfoDto getById(UUID id) {
        final String sql = """
                SELECT NEW com.example.delivery.deliveryApp.Core.Application.query.getOrders.OrderInfoDto(o.id, o.courierId, c.location)
                FROM OrderEntity o
                JOIN FETCH CourierEntity c
                ON o.courierId = c.id 
                WHERE o.id = :id""";

        return manager.createQuery(sql, OrderInfoDto.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<OrderInfoDto> getAllNotCompleted() {
        final String jpql = """
                SELECT NEW com.example.delivery.deliveryApp.Core.Application.query.getOrders.OrderInfoDto(o.id, o.courierId, c.location)
                FROM OrderEntity o 
                JOIN FETCH CourierEntity c
                ON o.courierId = c.id
                WHERE o.orderStatus <> :completedStatus""";

        TypedQuery<OrderInfoDto> query = manager.createQuery(jpql, OrderInfoDto.class);
        query.setParameter("completedStatus", OrderStatus.COMPLETED);

        return query.getResultList();
    }
}
