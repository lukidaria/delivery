package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Order.Repository;

import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.OrderStatus;
import com.example.delivery.deliveryApp.Core.Ports.OrderRepository;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Order.Mapper.OrderMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class OrderRepositoryImpl implements OrderRepository {

    private final OrderJpaRepository repository;
    private final OrderMapper mapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Order save(Order order) {
        var entity = mapper.toEntity(order);
        return mapper.toDomain(repository.saveAndFlush(entity));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Order order) {
        var reference = repository.getReferenceById(order.getId());
        var entity = mapper.updateEntity(order, reference);
        repository.saveAndFlush(entity);
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Order getById(UUID id) {
        return repository.findById(id)
                .map(mapper::toDomain)
                .orElse(null);
    }

    @Override
    public List<Order> getAllNotAssigned() {
        return Optional.of(mapper.toDomain(repository.findAllByOrderStatus(OrderStatus.CREATED))).orElse(Collections.emptyList());
    }

    @Override
    public List<Order> getAllAssigned() {
        return Optional.of(mapper.toDomain(repository.findAllByOrderStatus(OrderStatus.ASSIGNED))).orElse(Collections.emptyList());
    }

    public List<Order> getAllNotCompleted(){
        var created = Optional.of(mapper.toDomain(repository.findAllByOrderStatus(OrderStatus.CREATED)))
                .orElse(Collections.emptyList());
        var assigned = Optional.of(mapper.toDomain(repository.findAllByOrderStatus(OrderStatus.ASSIGNED)))
                .orElse(Collections.emptyList());
        return Stream.concat(created.stream(), assigned.stream())
                .collect(Collectors.toList());
    }

    @Override
    public Order getByCourierId(UUID courierId) {
        return Optional.of(mapper.toDomain(repository.findByCourierId(courierId))).orElse(null);
    }
}
