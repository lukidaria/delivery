package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier.Entity;

import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Transport;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.BaseEntity;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter.CourierStatusConverter;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter.LocationConverter;
import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.CourierStatus;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter.TransportConverter;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "couriers")
public class CourierEntity extends BaseEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "status_id")
    @Convert(converter = CourierStatusConverter.class)
    private CourierStatus courierStatus;

    @Column(name = "transport_id")
    @Convert(converter = TransportConverter.class)
    private Transport transport;

    @Column(name = "current_location")
    @Convert(converter = LocationConverter.class)
    private Location location;

}
