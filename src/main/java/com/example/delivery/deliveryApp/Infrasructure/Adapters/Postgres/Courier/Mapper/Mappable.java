package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier.Mapper;


public interface Mappable<Entity, Domain> {
    Domain toDomain(Entity entity);

    Entity toEntity(Domain domain);
}

