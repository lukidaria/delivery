package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Order.Mapper;

import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Order.Entity.OrderEntity;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier.Mapper.Mappable;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;
import java.util.Optional;

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface OrderMapper extends Mappable<OrderEntity, Order> {

    Order toDomain(OrderEntity entity);

    List<Order> toDomain(List<OrderEntity> entities);

    OrderEntity toEntity(Order domain);

    default OrderEntity updateEntity(Order entity, OrderEntity targetEntity) {
        Optional.of(entity)
                .map(Order::getOrderStatus).ifPresent(targetEntity::setOrderStatus);
        Optional.of(entity)
                .map(Order::getWeight).ifPresent(targetEntity::setWeight);
        Optional.of(entity)
                .map(Order::getLocation).ifPresent(targetEntity::setDeliveryLocation);
        Optional.of(entity)
                .map(Order::getCourierId).ifPresent(targetEntity::setCourierId);
        return targetEntity;
    }
}
