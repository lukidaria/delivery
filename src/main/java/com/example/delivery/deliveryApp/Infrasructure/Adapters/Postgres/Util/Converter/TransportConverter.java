package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter;

import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Transport;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter
public class TransportConverter implements AttributeConverter<Transport, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Transport attribute) {
        try {
            return attribute.getId();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Transport convertToEntityAttribute(Integer dbData) {
        return Transport.of(dbData);
    }
}
