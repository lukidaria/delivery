package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier;

import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.CourierStatus;
import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Courier;
import com.example.delivery.deliveryApp.Core.Ports.CourierRepository;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier.Mapper.CourierMapper;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier.Repository.CourierJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CourierRepositoryImpl implements CourierRepository {

    private final CourierJpaRepository repository;
    private final CourierMapper mapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Courier save(Courier courier) {
        var entity = mapper.toEntity(courier);
        return mapper.toDomain(repository.saveAndFlush(entity));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Courier courier) {
        var reference = repository.getReferenceById(courier.getId());
        var entity = mapper.updateEntity(courier, reference);
        repository.saveAndFlush(entity);
    }

    @Override
    @Transactional(readOnly = true, rollbackFor = Exception.class)
    public Courier getById(UUID id) {
        return repository.findById(id)
                .map(mapper::toDomain)
                .orElse(null);
    }

    @Override
    public List<Courier> getAllReady() {
        return Optional.of(mapper.toDomain(repository.findAllByCourierStatus(CourierStatus.READY)))
                .orElse(Collections.emptyList());
    }

    @Override
    public List<Courier> getAllBusy() {
        return Optional.of(mapper.toDomain(repository.findAllByCourierStatus(CourierStatus.BUSY)))
                .orElse(Collections.emptyList());
    }
}

