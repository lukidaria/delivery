package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier.Repository;

import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Courier.Entity.CourierEntity;
import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.CourierStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CourierJpaRepository extends JpaRepository<CourierEntity, UUID> {

    List<CourierEntity> findAllByCourierStatus(CourierStatus status);
}
