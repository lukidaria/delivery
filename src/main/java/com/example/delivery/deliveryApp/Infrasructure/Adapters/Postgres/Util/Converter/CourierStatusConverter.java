package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter;

import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.CourierStatus;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter
public class CourierStatusConverter implements AttributeConverter<CourierStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(CourierStatus attribute) {
        try {
            return attribute.getId();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public CourierStatus convertToEntityAttribute(Integer dbData) {
        return CourierStatus.of(dbData);
    }
}
