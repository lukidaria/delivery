package com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Order.Entity;

import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter.LocationConverter;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.OrderStatus;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Weight;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.BaseEntity;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter.WeightConverter;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Getter
@Setter
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "orders")
public class OrderEntity extends BaseEntity {

    @Column(name = "courier_id")
    private UUID courierId;

    @Column(name = "status_id")
    private OrderStatus orderStatus;

    @Column(name = "delivery_location")
    @Convert(converter = LocationConverter.class)
    private Location deliveryLocation;

    @Column(name = "weight")
    @Convert(converter = WeightConverter.class)
    private Weight weight;
}
