package com.example.delivery.deliveryApp.Core.Application.command.finishWork;

import com.example.delivery.deliveryApp.Core.Ports.CourierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class FinishWorkHandler {

    private final CourierRepository courierRepository;

    public void finish(FinishWorkCommand command) {
        var courierId = command.courierId();
        var courier = courierRepository.getById(courierId);
        courier.finishDay();
        courierRepository.update(courier);
    }
}
