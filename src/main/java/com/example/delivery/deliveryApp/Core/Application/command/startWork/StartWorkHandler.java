package com.example.delivery.deliveryApp.Core.Application.command.startWork;

import com.example.delivery.deliveryApp.Core.Application.command.finishWork.FinishWorkCommand;
import com.example.delivery.deliveryApp.Core.Ports.CourierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StartWorkHandler {

    private final CourierRepository courierRepository;

    public void start(StartWorkCommand command) {
        var courierId = command.courierId();
        var courier = courierRepository.getById(courierId);
        courier.startDay();
        courierRepository.update(courier);
    }
}
