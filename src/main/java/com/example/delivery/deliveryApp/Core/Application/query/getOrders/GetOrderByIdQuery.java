package com.example.delivery.deliveryApp.Core.Application.query.getOrders;

import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public record GetOrderByIdQuery(@NotNull UUID orderId) {
}
