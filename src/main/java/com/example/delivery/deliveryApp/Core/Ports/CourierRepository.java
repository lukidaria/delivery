package com.example.delivery.deliveryApp.Core.Ports;

import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Courier;

import java.util.List;
import java.util.UUID;

public interface CourierRepository {

    Courier save(Courier courier);

    void update(Courier courier);

    Courier getById(UUID id);

    List<Courier> getAllReady();

    List<Courier> getAllBusy();

}
