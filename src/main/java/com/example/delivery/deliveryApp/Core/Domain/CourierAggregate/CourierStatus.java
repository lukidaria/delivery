package com.example.delivery.deliveryApp.Core.Domain.CourierAggregate;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
public enum CourierStatus {

    NOT_AVAILABLE(1, "NotAvailable"),
    READY(2, "Ready"),
    BUSY(3, "Busy");

    private final int id;
    private final String value;

    public static boolean isNotAvailable(CourierStatus status) {
        return NOT_AVAILABLE.equals(status);
    }

    public static boolean isReady(CourierStatus status) {
        return READY.equals(status);
    }

    public static boolean isBusy(CourierStatus status) {
        return BUSY.equals(status);
    }

    public static CourierStatus of(Integer id) {
        return Stream.of(values())
                .filter(t -> t.getId() == id)
                .findAny()
                .orElse(null);
    }
}
