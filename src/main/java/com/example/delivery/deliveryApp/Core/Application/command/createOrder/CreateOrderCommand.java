package com.example.delivery.deliveryApp.Core.Application.command.createOrder;

import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;

import java.util.UUID;

public record CreateOrderCommand(Order order) {
}
