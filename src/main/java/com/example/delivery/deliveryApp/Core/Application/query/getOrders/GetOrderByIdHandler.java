package com.example.delivery.deliveryApp.Core.Application.query.getOrders;

import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;
import com.example.delivery.deliveryApp.Core.Ports.OrderInfoRepository;
import com.example.delivery.deliveryApp.Core.Ports.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GetOrderByIdHandler {

    private final OrderInfoRepository repository;

    public OrderInfoDto get(GetOrderByIdQuery query) {
        return repository.getById(query.orderId());
    }
}
