package com.example.delivery.deliveryApp.Core.Application.command.createOrder;

import com.example.delivery.deliveryApp.Core.Application.command.assignOrder.AssignOrderCommand;
import com.example.delivery.deliveryApp.Core.Ports.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CreateOrderHandler {


    private final OrderRepository orderRepository;

    @Transactional(rollbackFor = Exception.class)
    public void create(CreateOrderCommand command) {
        var order = command.order();
        orderRepository.save(order);
    }
}
