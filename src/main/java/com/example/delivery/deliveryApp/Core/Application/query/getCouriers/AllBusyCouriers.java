package com.example.delivery.deliveryApp.Core.Application.query.getCouriers;

import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Courier;
import com.example.delivery.deliveryApp.Core.Ports.CourierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AllBusyCouriers {

    private final CourierRepository repository;

    public List<Courier> get() {
        return repository.getAllBusy();
    }
}
