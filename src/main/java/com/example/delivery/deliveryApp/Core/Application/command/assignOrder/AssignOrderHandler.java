package com.example.delivery.deliveryApp.Core.Application.command.assignOrder;

import com.example.delivery.deliveryApp.Core.DomainServices.DispatchService;
import com.example.delivery.deliveryApp.Core.Ports.CourierRepository;
import com.example.delivery.deliveryApp.Core.Ports.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AssignOrderHandler {

    private final DispatchService dispatchService;
    private final CourierRepository courierRepository;
    private final OrderRepository orderRepository;


    @Transactional(rollbackFor = Exception.class)
    public void assignTo(AssignOrderCommand assignOrderCommand) {
        var order = orderRepository.getById(Optional.of(assignOrderCommand.orderId()).orElse(null));
        var couriers = courierRepository.getAllReady();
        var courier = dispatchService.dispatch(order, couriers);
        courier.assignOrder(order);
        courierRepository.update(courier);
        orderRepository.update(order);
    }

}
