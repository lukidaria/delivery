package com.example.delivery.deliveryApp.Core.Domain.OrderAggregate;

import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Weight;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter
@AllArgsConstructor
public class Order {

    private UUID id;
    private UUID courierId;
    private Location location;
    private Weight weight;
    private OrderStatus orderStatus;

    public static Order create(UUID id, Location location, Weight weight) {
        return new Order(id, null, location, weight, OrderStatus.CREATED);
    }

    public void assignCourier(UUID courierId) {
        this.courierId = courierId;
        this.orderStatus = OrderStatus.ASSIGNED;
    }

    public void completeOrder(){
        if (OrderStatus.ASSIGNED.equals(this.orderStatus)) {
            this.orderStatus = OrderStatus.COMPLETED;
        } else {
            throw new IllegalArgumentException("Order was not assigned");
        }


    }
}
