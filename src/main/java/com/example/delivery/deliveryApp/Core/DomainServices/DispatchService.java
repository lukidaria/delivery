package com.example.delivery.deliveryApp.Core.DomainServices;

import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.Courier;
import com.example.delivery.deliveryApp.Core.Domain.CourierAggregate.CourierStatus;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.OrderStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;


@Service
public class DispatchService {

    @Transactional
    public Courier dispatch(Order order, List<Courier> couriers) {
        if (OrderStatus.CREATED.equals(order.getOrderStatus())) {
            return couriers.stream()
                    .filter(courier -> CourierStatus.isReady(courier.getCourierStatus()))
                    .filter(courier -> courier.getTransport().isCapacityCanDeliver(order.getWeight()))
                    .min(Comparator.comparingInt(courier -> courier.getStepsToLocation(order)))
                    .orElse(null);
        }
        return null;
    }
}
