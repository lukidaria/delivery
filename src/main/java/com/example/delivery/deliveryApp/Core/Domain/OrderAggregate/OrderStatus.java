package com.example.delivery.deliveryApp.Core.Domain.OrderAggregate;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum OrderStatus {

    CREATED(1,"Created"),
    ASSIGNED(2, "Assigned"),
    COMPLETED(3, "Completed");

    private final int id;
    private final String value;
}
