package com.example.delivery.deliveryApp.Core.Application.command.startWork;

import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public record StartWorkCommand(@NotNull UUID courierId) {
}
