package com.example.delivery.deliveryApp.Core.Application.command.moveToLocation;

import com.example.delivery.deliveryApp.Core.Ports.CourierRepository;
import com.example.delivery.deliveryApp.Core.Ports.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MoveToLocationHandler {

    private final CourierRepository courierRepository;
    private final OrderRepository orderRepository;

    @Transactional(rollbackFor = Exception.class)
    public void moveTo(MoveToLocationCommand moveToLocationCommand) {
        var courierId = Optional.of(moveToLocationCommand.courierId()).orElse(null);
        var courier = courierRepository.getById(courierId);
        var order = orderRepository.getByCourierId(courierId);
        var result = courier.moveToLocation(order);
        if (result) {
            orderRepository.update(order);
        }
        courierRepository.update(courier);
    }
}
