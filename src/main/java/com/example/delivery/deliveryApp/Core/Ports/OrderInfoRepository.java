package com.example.delivery.deliveryApp.Core.Ports;

import com.example.delivery.deliveryApp.Core.Application.query.getOrders.OrderInfoDto;

import java.util.List;
import java.util.UUID;

public interface OrderInfoRepository {

    OrderInfoDto getById(UUID orderInfoId);

    List<OrderInfoDto> getAllNotCompleted();
}
