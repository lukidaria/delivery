package com.example.delivery.deliveryApp.Core.Domain.CourierAggregate;

import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Weight;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@Getter
@RequiredArgsConstructor
public enum Transport {

    PEDESTRIAN(1, "pedestrian", 1, new Weight(1)),
    BICYCLE(2, "bicycle", 2, new Weight(4)),
    SCOOTER(3, "scooter", 3, new Weight(4)),
    CAR(4, "car", 4, new Weight(8));

    private final int id;
    private final String name;
    private final int speed;
    private final Weight capacity;

    public boolean isCapacityCanDeliver(Weight weight) {
        return weight.value() <= this.capacity.value();
    }

    public static Transport of(Integer id) {
        return Stream.of(values())
                .filter(t -> t.getId() == id)
                .findAny()
                .orElse(null);
    }
}
