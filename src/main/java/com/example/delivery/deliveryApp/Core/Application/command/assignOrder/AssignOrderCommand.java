package com.example.delivery.deliveryApp.Core.Application.command.assignOrder;

import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public record AssignOrderCommand(@NotNull UUID orderId){

}
