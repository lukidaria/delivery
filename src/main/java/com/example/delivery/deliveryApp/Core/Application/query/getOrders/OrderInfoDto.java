package com.example.delivery.deliveryApp.Core.Application.query.getOrders;

import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import com.example.delivery.deliveryApp.Infrasructure.Adapters.Postgres.Util.Converter.LocationConverter;
import jakarta.persistence.Convert;
import jakarta.persistence.Converter;
import lombok.*;

import java.util.UUID;
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderInfoDto{

    private UUID id;
    private UUID courierId;
    private Location courierLocation;

}
