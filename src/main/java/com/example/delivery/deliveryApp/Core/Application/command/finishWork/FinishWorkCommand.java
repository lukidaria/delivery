package com.example.delivery.deliveryApp.Core.Application.command.finishWork;

import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public record FinishWorkCommand(@NotNull UUID courierId) {
}
