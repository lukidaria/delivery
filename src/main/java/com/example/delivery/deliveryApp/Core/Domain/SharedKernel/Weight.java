package com.example.delivery.deliveryApp.Core.Domain.SharedKernel;

import java.util.Objects;


public record Weight(int value) implements Comparable<Weight> {

    public Weight {
        validateValue(value);
    }

    private void validateValue(int value){
        if (value <= 0) {
            throw new IllegalArgumentException("Value should be positive.");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Weight weight = (Weight) o;
        return weight.value == value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public int compareTo(Weight other) {
        return Integer.compare(this.value, other.value);
    }
}
