package com.example.delivery.deliveryApp.Core.Domain.CourierAggregate;

import com.example.delivery.deliveryApp.Core.Domain.SharedKernel.Location;
import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@AllArgsConstructor
@Getter
public class Courier {

    @NotNull
    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private Transport transport;
    @NotNull
    private Location location;
    @NotNull
    private CourierStatus courierStatus;

    public Courier create(String name, Transport transport) {
        return new Courier(UUID.randomUUID(), name, transport, new Location(1, 1), CourierStatus.READY);
    }

    public void startDay() {
        if (CourierStatus.isNotAvailable(courierStatus)) {
            throw new IllegalArgumentException("CourierStatus is not available");
        }
        this.courierStatus = CourierStatus.READY;
    }

    public void finishDay() {
        if (!CourierStatus.isReady(courierStatus)) {
            throw new IllegalArgumentException("CourierStatus is busy");
        }
        this.courierStatus = CourierStatus.NOT_AVAILABLE;
    }

    public void assignOrder(Order order) {
        if (!CourierStatus.isReady(courierStatus)) {
            throw new IllegalArgumentException("CourierStatus is busy or not available for assigning order");
        }
        this.courierStatus = CourierStatus.BUSY;
        order.assignCourier(id);
    }

    public boolean moveToLocation(Order order) {
        var step = transport.getSpeed();
        if (step >= location.distanceToFull(order.getLocation())) {
            location = order.getLocation();
            courierStatus = CourierStatus.READY;
            order.completeOrder();
            return true;
        } else {
            location = location.moveToTargetLocation(order.getLocation(), step);
            return false;
        }
    }

    public int getStepsToLocation(Order order) {
        var distanceToOrder = location.distanceToFull(order.getLocation());
        //возвращаем округленное в большую сторону количество шагов до заказа
        return (int) Math.ceil((double) distanceToOrder / (double) transport.getSpeed());

    }

}
