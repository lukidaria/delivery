package com.example.delivery.deliveryApp.Core.Application.command.moveToLocation;

import jakarta.validation.constraints.NotNull;

import java.util.UUID;

public record MoveToLocationCommand(@NotNull UUID courierId){

}
