package com.example.delivery.deliveryApp.Core.Domain.SharedKernel;

import static java.lang.String.format;


public record Location(int xValue, int yValue) {
    private static final int MAX_COORDINATE_VALUE = 10;
    private static final int MIN_COORDINATE_VALUE = 1;

    public static final Location MIN_LOCATION = create(MIN_COORDINATE_VALUE, MIN_COORDINATE_VALUE);
    public static final Location MAX_LOCATION = create(MAX_COORDINATE_VALUE, MAX_COORDINATE_VALUE);

    public Location {
        validateCoordinate(xValue);
        validateCoordinate(yValue);
    }

    public static Location create(int xValue, int yValue) {
        return new Location(xValue, yValue);
    }

    private static void validateCoordinate(int value) {
        if (value < MIN_COORDINATE_VALUE || value > MAX_COORDINATE_VALUE) {
            throw new IllegalArgumentException(
                    format("Значение координаты должно быть между '%d' and '%d'. Текущее значение  '%d'.",
                            MIN_COORDINATE_VALUE, MAX_COORDINATE_VALUE, value)
            );
        }
    }

    public int distanceToX(Location targetlLocation) {
        return Math.abs(this.xValue - targetlLocation.xValue);
    }

    public int distanceToY(Location targetlLocation) {
        return Math.abs(this.yValue - targetlLocation.yValue);
    }

    public int distanceToFull(Location targetlLocation) {
        return distanceToX(targetlLocation) + distanceToY(targetlLocation);
    }

    public Location moveToTargetLocation(Location targetLocation, int steps) {
        int newX = xValue;
        int newY = yValue;
        Location location = null;
        while (steps > 0) {
            if (distanceToY(targetLocation) > 0) {
                newY += 1;
                location = new Location(newX, newY);
                steps--;
            }
            if (distanceToX(targetLocation) > 0 && steps > 0) {
                newX += 1;
                location = new Location(newX, newY);
                steps--;
            }
        }
        return location;
    }
}
