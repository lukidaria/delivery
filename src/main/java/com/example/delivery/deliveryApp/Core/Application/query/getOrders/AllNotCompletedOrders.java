package com.example.delivery.deliveryApp.Core.Application.query.getOrders;

import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;
import com.example.delivery.deliveryApp.Core.Ports.OrderInfoRepository;
import com.example.delivery.deliveryApp.Core.Ports.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AllNotCompletedOrders {

    private final OrderInfoRepository repository;

    public List<OrderInfoDto> get() {
        return repository.getAllNotCompleted();
    }

}
