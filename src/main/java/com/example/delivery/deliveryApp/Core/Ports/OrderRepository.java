package com.example.delivery.deliveryApp.Core.Ports;

import com.example.delivery.deliveryApp.Core.Domain.OrderAggregate.Order;

import java.util.List;
import java.util.UUID;

public interface OrderRepository {

    Order save(Order order);

    void update(Order order);

    Order getById(UUID id);

    List<Order> getAllNotAssigned();

    List<Order> getAllAssigned();

    List<Order> getAllNotCompleted();

    Order getByCourierId(UUID courierId);
}
